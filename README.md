# The Receiver

The receiver is a simple API endpoint for receiving data.

The same endpoint is also used for displaying captured data.

#### Intended Use Cases
  - Specifically for sending data captured via a XSS attack
  - Could also be used to send off user names and passwords captured through other means
  - To serve as a general purpose data collection endpoint for any data (non-security/maliciously related) 

#### Disclaimer
`While The Receiver is created strictly for educational and academic purposes, it is free and Open Source. This means it can be obtained and used by anyone even individuals with malicious intent. Use of this software for ANYTHING illegal is not condoned, and creator of this software is not responsible for anyone who uses it as defined in the MIT licensce agreement provided with this software.`
